#include "Linked_list.hpp"

Linked_list::Linked_list(std::string attribute)
 {
 // Set the attribute and initialize the next_ptr to point to end of list
  set(attribute);
  this->next_ptr = NULL;
 } 


void Linked_list::set(std::string attribute)
 {
  this->attribute = attribute;
 }


std::string Linked_list::get()
 {
  return attribute;
 }


Linked_list* Linked_list::next()
 {
  return this->next_ptr;
 }


Linked_list* Linked_list::insert_at_index(unsigned int index, Linked_list *to_insert)
 {
 // to_insert has to be a single node, NOT a list.
   if(to_insert->next() != NULL) return NULL;

 // basecase: prepend
   if(index == 0)
    {
      to_insert->next_ptr = this;
     return to_insert;
    }

  Linked_list *before = this;
   for(unsigned int i=index; i>1; --i) // i == 0 <=> before points to element to delete
    {
    // iterate through list
     before = before->next();

    // fail in case of premature end-of-list
      if(before == NULL) return NULL;
    }

 // make to_insert point to the element after the one it is inserted after
  to_insert->next_ptr = before->next_ptr;

 // make before-node point to the to_insert-node
  before->next_ptr = to_insert;

 // root stayed the same; return it
  return this;
 }


Linked_list* Linked_list::remove_at_index(unsigned int index)
 {
 // Edge-case: if the root-node is removed, we have to return the node after it
   if(index == 0)
    {
    Linked_list *ret = this->next();
     delete this;
     return ret;
    }

 // pointer to the node before the one that has to be deleted
 Linked_list *before = this;
   for(unsigned int i=index; i>1; --i) // i == 0 <=> before points to element to delete
    {
    // iterate through list
     before = before->next();

    // fail in case of premature end-of-list
      if(before == NULL) return NULL;
    }

 // Now we have the element we have to delete
 Linked_list *to_del = before->next();
 // fail in case of premature end-of-list
   if(to_del == NULL) return NULL;

 // Make the one before the deleted node point to the node after the deleted node
  before->next_ptr = to_del->next();

 // Actually delete it
  delete to_del;

 // root stayed the same; return it
  return this;
 }


