#ifndef __LINKED_LIST_HPP__
#define __LINKED_LIST_HPP__

#include <string>
class Linked_list
 {
  std::string attribute;
  Linked_list *next_ptr;

 public:
  Linked_list(std::string attribute);
  void set(std::string attribute);
  std::string get();
  Linked_list* next();
  Linked_list* insert_at_index(unsigned int index, Linked_list *to_insert);
  Linked_list* remove_at_index(unsigned int index);
 };

#endif

