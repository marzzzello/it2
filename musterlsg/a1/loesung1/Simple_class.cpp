#include "Simple_class.hpp"

Simple_class::Simple_class(int attribute)
 {
  set(attribute);
 } 

void Simple_class::set(int attribute)
 {
  this->attribute = attribute;
 }

int Simple_class::get()
 {
  return attribute;
 }

