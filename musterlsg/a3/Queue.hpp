#ifndef __QUEUE_HPP__
#define __QUEUE_HPP__

#include "Linked_list.hpp"

class Queue
 {
 Linked_list *first_node;
 public:
  Queue();
  void push(std::string attribute);
  std::string pop();
  bool has_next();
 }; 

#endif

