#include "Queue.hpp"

Queue::Queue()
 {
  first_node = NULL;
 }

void Queue::push(std::string attribute)
 {
   if(first_node == NULL)
    {											// if queue empty so far
     first_node = new Linked_list(attribute);	// set linked list as queue
    }
   else
    {
    Linked_list *last_node = first_node;	// set last element of list = first element of queue
      while(last_node->next() != NULL)
       {
        last_node = last_node->next();		// and append the other elements
       }
     last_node->insert_at_index(1, new Linked_list(attribute));
    }
 }

std::string Queue::pop()
 {
 std::string ret = first_node->get();			// return first element in queue
  first_node = first_node->remove_at_index(0);	// set next node as new first node
  return ret;
 }

bool Queue::has_next()
 {
  return first_node != NULL;
 }

