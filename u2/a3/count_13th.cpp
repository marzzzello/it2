#include "count_13th.hpp"

Weekday_statistics count_13th (int start_year , int year_offset ){
	Weekday_statistics w; //return Objekt

	//Date Generator:
	
	int daysConfig=0;

	for (int yearLoop=start_year; yearLoop<(start_year+year_offset); yearLoop++){
		int year=yearLoop;
		for (int month=1; month <= 12; month++){
			if (month==9 || month==4 || month == 6 || month == 11){
				daysConfig=30;
			}
				
			else if (month==02){//Für Februar muss das Schaltjahr berücksichtigt werden:
				if (year%400==0){ //Fall: Schaltjahr, durch 400 teilbar.
					daysConfig=29;
				}
				else if (year%100==0)//Fall: kein Schaltjahr da durch 100 Teilbar
				{
					daysConfig=28;
				}
				else if (year%4==0)//Fall: Schaltjahr, durch 4 teilbar.
				{
					daysConfig=29;
				}
				else {
					daysConfig=28;//Kein Schaltjahr.
				}
			}
			else {
				daysConfig=31; //Für alle anderen Monate außer Februar,September, April, Juni und November
			}
			for (int day=1;day<=daysConfig;day++){
				//Wochentag zu aktuellem Datum herausfinden 
				if (day==13){ //Falls der Tag der 13. eines Monats ist!!!!!!!!!!!!!!!!!
					int j=0,k=0,h=0;

					if (month<3) 
					{
					month=month+12;
					year--; //grrrrrrrrrrrrrrrrrrrrrrrr
					}

					j=year/100;
					k=year%100;

					h=day+k-2*j;
					h=h+k/4;
					h=h+j/4;
					h=h+((month+1)*26/10);
					//h-=2; //an dieses Format anpassen (Montag = 0 statt Samstag =0)
					h=h%7;

						switch (h)
						{
					case 0:
						w.set_saturday_value(w.get_saturday_value()+1);
						break;
					case 1:
						w.set_sunday_value(w.get_sunday_value()+1);
						break;
					case 2:
						w.set_monday_value(w.get_monday_value()+1);
						break;
					case 3:
						w.set_tuesday_value(w.get_tuesday_value()+1);
						break;
					case 4:
						w.set_wednesday_value(w.get_wednesday_value()+1);
						break;
					case 5:
						w.set_thursday_value(w.get_thursday_value()+1);
						break;
					case 6:
						w.set_sunday_value(w.get_sunday_value()+1);
						break;

					}
				}
  			}
		}
	}
}



