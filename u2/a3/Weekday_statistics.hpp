#ifndef __WEEKDAY_STATISTICS_HPP__
#define __WEEKDAY_STATISTICS_HPP__

class Weekday_statistics{
private:
	int freq [7];
	
public:

	Weekday_statistics();

	void set_monday_value(int);
  	void set_tuesday_value(int);
  	void set_wednesday_value(int);
  	void set_thursday_value(int);
  	void set_friday_value(int);
  	void set_saturday_value(int);
  	void set_sunday_value(int);

	int get_monday_value();
 	int get_tuesday_value();
 	int get_wednesday_value();
 	int get_thursday_value();
 	int get_friday_value();
 	int get_saturday_value();
 	int get_sunday_value();


};

#endif
