
#include "Weekday_statistics.hpp"

Weekday_statistics::Weekday_statistics() {
	for (int i=0;i<7;i++)
		freq[i]=0;	//Alle Häufigkeiten mit 0 inititalisieren
}


	void Weekday_statistics::set_monday_value(int toSet)
		 {
		 	freq[0]=toSet;
		 }
  	void Weekday_statistics::set_tuesday_value(int toSet)
  		 {
  		 	freq[1]=toSet;
  		 }
  	void Weekday_statistics::set_wednesday_value(int toSet)
  		 {
  		 	freq[2]=toSet;
  		 }
  	void Weekday_statistics::set_thursday_value(int toSet)
  		 {
  		 	freq[3]=toSet;
  		 }
  	void Weekday_statistics::set_friday_value(int toSet)
  		 {
  		 	freq[4]=toSet;
  		 }
  	void Weekday_statistics::set_saturday_value(int toSet)
  		 {
  		 	freq[5]=toSet;
  		 }
  	void Weekday_statistics::set_sunday_value(int toSet)
  		 {
  		 	freq[6]=toSet;
  		 }

	int Weekday_statistics::get_monday_value() {return freq[0];}
 	int Weekday_statistics::get_tuesday_value() {return freq[1];}
 	int Weekday_statistics::get_wednesday_value() {return freq[2];}
 	int Weekday_statistics::get_thursday_value() {return freq[3];}
 	int Weekday_statistics::get_friday_value() {return freq[4];}
 	int Weekday_statistics::get_saturday_value() {return freq[5];}
 	int Weekday_statistics::get_sunday_value() {return freq[6];}
