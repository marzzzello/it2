O(1)
lim (n->unendlich) (1+1/N)*(1+2/N) = lim (n->unendlich)  1 + 1/N + 2/N + 1/N² = 1
worst case: N=1