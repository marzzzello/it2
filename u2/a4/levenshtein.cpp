#include "levenshtein.hpp"

#include <iostream>
using namespace std;

int minimal(int *D, std::string *original, std::string *katze, int i, int j)
 {
  //Hilfsstrukt zum Minimum berechnen
   struct GetMin
    {
      static int min(int a, int b)//Minimum für 2 Werte
       {
         if(b > a)    //ist a kleiner als b?
          return a;   //dann ist a das Minimum
        return b;   //Sonst b
       }

      static int min(int a, int b, int c) //Minimum für 3 Werte
       {
        return min(min(a, b), c);
       }

    };

 int n = katze->size();

 
   if((*original)[i-1] == (*katze)[j-1])
      return GetMin::min(  D[(i-1)*(n+1)+(j-1)], D[(i-1)*(n+1)+j] +1, D[i*(n+1)+(j-1)] +1);
    
   else
      return GetMin::min(  D[(i-1)*(n+1)+(j-1)] +1, D[(i-1)*(n+1)+j] +1, D[i*(n+1)+(j-1)] +1);
}


int levenshtein_distance(std::string original, std::string katze)
 {
//Länge der Strings abspeichern:
 int m = original.size();
 int n = katze.size();

  //Matrix erstellen
 int *D = new int[(m+1)*(n+1)];

 // Init 0,0 
  D[0] = 0;

   for(int j=1, j_end=n; j <= j_end; ++j) 
      D[0*(n+1) + j] = j;

    //Hauptschleife des Alg.
   for(int i=1, i_end=m; i <= i_end; ++i)
    {
     D[i*(n+1) + 0] = i;
      for(int j=1, j_end=n; j <= j_end; ++j)
       {
        D[i*(n+1) + j] = minimal(D, &original, &katze, i, j); //Aufruf der Hilfsfkt.
       }
    }

  int ret = D[m*(n+1) + n]; //Return-Wert speichren damit 
  delete [] D;              //D gelöscht werden kann
  return ret;
 }

