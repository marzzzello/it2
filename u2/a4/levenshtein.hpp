#ifndef __LEVENSHTEIN_HPP__
#define __LEVENSHTEIN_HPP__

#include <string>

int minimal(int *D, std::string *original, std::string *katze, int i, int j);
int levenshtein_distance(std::string original, std::string katze);

#endif
