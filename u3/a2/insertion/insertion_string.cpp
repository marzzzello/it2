#include "insertion_string.hpp"

#include <iostream>

std::vector<std::string> insertion_sort (std::vector<std::string> strings, char c)
{
	// nutze die size() Funktion um die Anzahl der Strings des Vektors zu bekommen
	int number_of_strings = strings.size();
	
	std::string tmp;
	int i, j, min_idx;

	//Verschiebe Grenze des unsortierten Teil
	for ( i = 0; i < number_of_strings-1; i++)
	{
		// Finde kleinstes element im unsortierten Teil
		 min_idx = i;
		for (j = i+1; j < number_of_strings; j++)
			if (compare_by_frequency(strings[j],strings[min_idx],c)) //true wenn j kleiner als min_idx
				min_idx = j; 	//Speichere Index von kleinerem Element ab

		//Tausche kleinstes Element mit erstem Element
		tmp = strings[min_idx];
		strings[min_idx] = strings[i];
		strings[i] = tmp;
	}

	// Sortierter Vektor wird zurückgegeben
	return strings;
}

int compare_by_frequency(std::string a, std::string b, char c) {
	
	// Gibt Anzahl des Chars an
	int chars_a = 0;
	int chars_b = 0;
	
	//Gehe den ganzen String a durch und falls das Char vorkommt erhöhe Counter
	for (int i = 0; i < a.size(); i++) {
		if (a[i] == c)
			chars_a++;
	}
	
	//Das selbe für b
	for (int i = 0; i < b.size(); i++) {
		if (b[i] == c)
			chars_b++;
	}

	//Es ist nur relevant ob a<b ist
	if (chars_a < chars_b) 
		return 1;
	else
		return 0;
}
