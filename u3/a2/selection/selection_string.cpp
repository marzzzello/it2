#include "selection_string.hpp"
#include <iostream>

std::vector<std::string> selection_sort (std::vector<std::string> strings, char c){
	int smallestIndex;
	std::string tmp;
	int number_of_strings = strings.size();

	//Gehe alle Elemente im Vektor durch
	for (int startIndex=0; startIndex < number_of_strings-1; ++startIndex) {
	    smallestIndex = startIndex;//set smallestIndex to the current index of array
		
		//Suche nach kleinerem Element im restlichen Array
		for (int cur=startIndex+1; cur < number_of_strings; cur++) {

			if (compare_by_frequency(strings[cur], strings[smallestIndex], c)) //true wenn cur kleiner als smallest
				smallestIndex = cur; //Speichere Index von kleinerem Element ab
		}
			//Tausche Elemente 
			tmp = strings[startIndex];
			strings[startIndex] = strings[smallestIndex];
			strings[smallestIndex] = tmp;
	}
	
	return strings;
}

int compare_by_frequency(std::string a, std::string b, char c) {
	
	// Gibt Anzahl des Chars an
	int chars_a = 0;
	int chars_b = 0;
	
	//Gehe den ganzen String a durch und falls das Char vorkommt erhöhe Counter
	for (int i = 0; i < a.size(); i++) {
		if (a[i] == c)
			chars_a++;
	}
	
	//Das selbe für b
	for (int i = 0; i < b.size(); i++) {
		if (b[i] == c)
			chars_b++;
	}

	//Es ist nur relevant ob a<b ist
	if (chars_a < chars_b) 
		return 1;
	else
		return 0;
}
