#!/bin/sh

# This file was autowritten by rmlint
# rmlint was executed from: /home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/
# Your command line was: rmlint

RMLINT_BINARY='(null)'

USER='marcel'
GROUP='users'

# Set to true on -n
DO_DRY_RUN=

# Set to true on -p
DO_PARANOID_CHECK=

# Set to true on -r
DO_CLONE_READONLY=

##################################
# GENERAL LINT HANDLER FUNCTIONS #
##################################

COL_RED='[0;31m'
COL_BLUE='[1;34m'
COL_GREEN='[0;32m'
COL_YELLOW='[0;33m'
COL_RESET='[0m'

print_progress_prefix() {
    echo -n $COL_BLUE
    printf "[% 4d%%] " $((PROGRESS_CURR * 100 / PROGRESS_TOTAL))
    echo -n $COL_RESET
    ((PROGRESS_CURR++))
}

handle_emptyfile() {
    print_progress_prefix
    echo $COL_GREEN 'Deleting empty file:' $COL_RESET "$1"
    if [ -z "$DO_DRY_RUN" ]; then
        rm -f "$1"
    fi
}

handle_emptydir() {
    print_progress_prefix
    echo $COL_GREEN 'Deleting empty directory:' $COL_RESET "$1"
    if [ -z "$DO_DRY_RUN" ]; then
        rmdir "$1"
    fi
}

handle_bad_symlink() {
    print_progress_prefix
    echo $COL_GREEN 'Deleting symlink pointing nowhere:' $COL_RESET "$1"
    if [ -z "$DO_DRY_RUN" ]; then
        rm -f "$1"
    fi
}

handle_unstripped_binary() {
    print_progress_prefix
    echo $COL_GREEN 'Stripping debug symbols of:' $COL_RESET "$1"
    if [ -z "$DO_DRY_RUN" ]; then
        strip -s "$1"
    fi
}

handle_bad_user_id() {
    print_progress_prefix
    echo $COL_GREEN 'chown' "$USER" $COL_RESET "$1"
    if [ -z "$DO_DRY_RUN" ]; then
        chown "$USER" "$1"
    fi
}

handle_bad_group_id() {
    print_progress_prefix
    echo $COL_GREEN 'chgrp' "$GROUP" $COL_RESET "$1"
    if [ -z "$DO_DRY_RUN" ]; then
        chgrp "$GROUP" "$1"
    fi
}

handle_bad_user_and_group_id() {
    print_progress_prefix
    echo $COL_GREEN 'chown' "$USER:$GROUP" $COL_RESET "$1"
    if [ -z "$DO_DRY_RUN" ]; then
        chown "$USER:$GROUP" "$1"
    fi
}

###############################
# DUPLICATE HANDLER FUNCTIONS #
###############################

original_check() {
    if [ ! -e "$2" ]; then
        echo $COL_RED "^^^^^^ Error: original has disappeared - cancelling....." $COL_RESET
        return 1
    fi

    if [ ! -e "$1" ]; then
        echo $COL_RED "^^^^^^ Error: duplicate has disappeared - cancelling....." $COL_RESET
        return 1
    fi

    # Check they are not the exact same file (hardlinks allowed):
    if [ "$1" = "$2" ]; then
        echo $COL_RED "^^^^^^ Error: original and duplicate point to the *same* path - cancelling....." $COL_RESET
        return 1
    fi

    # Do double-check if requested:
    if [ -z "$DO_PARANOID_CHECK" ]; then
        return 0
    else
        if $RMLINT_BINARY -p --equal "$1" "$2"; then
            return 0
        else
            echo $COL_RED "^^^^^^ Error: files no longer identical - cancelling....." $COL_RESET
            return 1
        fi
    fi
}

cp_hardlink() {
    print_progress_prefix
    echo $COL_YELLOW 'Hardlinking to original:' "$1" $COL_RESET
    if original_check "$1" "$2"; then
        if [ -z "$DO_DRY_RUN" ]; then
            # If it's a directory cp will create a new copy into
            # the destination path. This would lead to more wasted space...
            if [ -d "$1" ]; then
                rm -rf "$1"
            fi
            cp --remove-destination --archive --link "$2" "$1"
        fi
    fi
}

cp_symlink() {
    print_progress_prefix
    echo $COL_YELLOW 'Symlinking to original:' "$1" $COL_RESET
    if original_check "$1" "$2"; then
        if [ -z "$DO_DRY_RUN" ]; then
            touch -mr "$1" "$0"
            if [ -d "$1" ]; then
                rm -rf "$1"
            fi
            cp --remove-destination --archive --symbolic-link "$2" "$1"
            touch -mr "$0" "$1"
        fi
    fi
}

cp_reflink() {
    print_progress_prefix
    # reflink $1 to $2's data, preserving $1's  mtime
    echo $COL_YELLOW 'Reflinking to original:' "$1" $COL_RESET
    if original_check "$1" "$2"; then
        if [ -z "$DO_DRY_RUN" ]; then
            touch -mr "$1" "$0"
            if [ -d "$1" ]; then
                rm -rf "$1"
            fi
            cp --archive --reflink=always "$2" "$1"
            touch -mr "$0" "$1"
        fi
    fi
}

clone() {
    print_progress_prefix
    # clone $1 from $2's data
    echo $COL_YELLOW 'Cloning to: ' "$1" $COL_RESET
    if [ -z "$DO_DRY_RUN" ]; then
        if [ -n "$DO_CLONE_READONLY" ]; then
            sudo $RMLINT_BINARY --btrfs-clone -r "$2" "$1"
        else
            $RMLINT_BINARY --btrfs-clone "$2" "$1"
        fi
    fi
}

skip_hardlink() {
    print_progress_prefix
    echo $COL_BLUE 'Leaving as-is (already hardlinked to original):' $COL_RESET "$1"
}

skip_reflink() {
    print_progress_prefix
    echo $COL_BLUE 'Leaving as-is (already reflinked to original):' $COL_RESET "$1"
}

user_command() {
    print_progress_prefix
    # You can define this function to do what you want:
    echo 'no user command defined.'
}

remove_cmd() {
    print_progress_prefix
    echo $COL_YELLOW 'Deleting:' $COL_RESET "$1"
    if original_check "$1" "$2"; then
        if [ -z "$DO_DRY_RUN" ]; then
            rm -rf "$1"
        fi
    fi
}

original_cmd() {
    print_progress_prefix
    echo $COL_GREEN 'Keeping: ' $COL_RESET "$1"
}

##################
# OPTION PARSING #
##################

ask() {
    cat << EOF

This script will delete certain files rmlint found.
It is highly advisable to view the script first!

Rmlint was executed in the following way:

   $ rmlint

Execute this script with -d to disable this informational message.
Type any string to continue; CTRL-C, Enter or CTRL-D to abort immediately
EOF
    read eof_check
    if [ -z "$eof_check" ]
    then
        # Count Ctrl-D and Enter as aborted too.
        echo $COL_RED "Aborted on behalf of the user." $COL_RESET
        exit 1;
    fi
}

usage() {
    cat << EOF
usage: $0 OPTIONS

OPTIONS:

  -h   Show this message.
  -d   Do not ask before running.
  -x   Keep rmlint.sh; do not autodelete it.
  -p   Recheck that files are still identical before removing duplicates.
  -r   Allow btrfs-clone to clone to read-only snapshots. (requires sudo)
  -n   Do not perform any modifications, just print what would be done. (implies -d and -x)
EOF
}

DO_REMOVE=
DO_ASK=

while getopts "dhxnrp" OPTION
do
  case $OPTION in
     h)
       usage
       exit 1
       ;;
     d)
       DO_ASK=false
       ;;
     x)
       DO_REMOVE=false
       ;;
     n)
       DO_DRY_RUN=true
       DO_REMOVE=false
       DO_ASK=false
       ;;
     r)
       DO_CLONE_READONLY=true
       ;;
     p)
       DO_PARANOID_CHECK=true
  esac
done

if [ -z $DO_ASK ]
then
  usage
  ask
fi

if [ ! -z $DO_DRY_RUN  ]
then
    echo $COL_YELLOW "////////////////////////////////////////////////////////////" $COL_RESET
    echo $COL_YELLOW "///" $COL_RESET "This is only a dry run; nothing will be modified! " $COL_YELLOW "///" $COL_RESET
    echo $COL_YELLOW "////////////////////////////////////////////////////////////" $COL_RESET
    echo
fi

######### START OF AUTOGENERATED OUTPUT #########

process_lint() {
original_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/buffy_vampire_slayer.hpp' # original
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a1/buffy_vampire_slayer.hpp' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/buffy_vampire_slayer.hpp' # duplicate
original_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/buffy_vampire_slayer.cpp' # original
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a1/buffy_vampire_slayer.cpp' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/buffy_vampire_slayer.cpp' # duplicate
original_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/tests.cpp' # original
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a1/tests.cpp' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/tests.cpp' # duplicate
original_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe2/insertion/tests.cpp' # original
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a2/insertion/tests.cpp' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe2/insertion/tests.cpp' # duplicate
original_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe2/insertion/Makefile' # original
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a2/insertion/Makefile' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe2/insertion/Makefile' # duplicate
original_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe2/selection/Makefile' # original
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a2/selection/Makefile' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe2/selection/Makefile' # duplicate
original_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/Makefile' # original
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a1new/Makefile' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/Makefile' # duplicate
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a1/Makefile' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/Makefile' # duplicate
original_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe2/selection/tests.cpp' # original
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a2/selection/tests.cpp' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe2/selection/tests.cpp' # duplicate
original_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe2/insertion/insertion_string.hpp' # original
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a2new/insertion/insertion_string.hpp' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe2/insertion/insertion_string.hpp' # duplicate
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a2/insertion/insertion_string.hpp' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe2/insertion/insertion_string.hpp' # duplicate
original_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe2/selection/selection_string.hpp' # original
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a2new/selection/selection_string.hpp' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe2/selection/selection_string.hpp' # duplicate
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a2/selection/selection_string.hpp' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe2/selection/selection_string.hpp' # duplicate
original_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/catch.hpp' # original
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe2/selection/catch.hpp' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/catch.hpp' # duplicate
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe2/insertion/catch.hpp' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/catch.hpp' # duplicate
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a2new/insertion/catch.hpp' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/catch.hpp' # duplicate
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a2new/selection/catch.hpp' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/catch.hpp' # duplicate
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a1new/catch.hpp' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/catch.hpp' # duplicate
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a2/insertion/catch.hpp' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/catch.hpp' # duplicate
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a2/selection/catch.hpp' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/catch.hpp' # duplicate
remove_cmd '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/a1/catch.hpp' '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/u3_2016/aufgabe1/catch.hpp' # duplicate
}                                              
                                               
PROGRESS_CURR=0                                
PROGRESS_TOTAL=32                          
                                               
######### END OF AUTOGENERATED OUTPUT #########
                                               
# Do all the hard work now.                    
process_lint                                   
                                               
if [ $PROGRESS_CURR -le $PROGRESS_TOTAL ]; then
    print_progress_prefix                      
    echo $COL_BLUE 'Done!' $COL_RESET          
fi                                             
                                               
if [ -z $DO_REMOVE ] && [ -z $DO_DRY_RUN ]     
then                                           
  echo "Deleting script " "$0"             
  rm -f '/home/marcel/rub/Informatik-2---Algorithmen-und-Datenstrukturen-(141321-SoSe17)/it2/u3/rmlint.sh';                                     
fi                                             
