#include "insertion_string.hpp"


#include <iostream>

std::vector<std::string> insertion_sort (std::vector<std::string> strings, char c){
	int j;
	std::string temp;
	
	// use the size() function to get the number of strings from the vector
	int number_of_strings = strings.size();
	
	// this is a standard insertion sort implementation
	for (int i = 1; i < number_of_strings; i++){
		j = i;
		
		// for comparing strings instead of integers we replace the standard compare with our compare_to function
		while (j > 0 && compare_by_frequency(strings[j], strings[j-1], c)){
			temp = strings[j];
			strings[j] = strings[j-1];
			strings[j-1] = temp;
			j--;
		}
	}
	
	return strings;
}

int compare_by_frequency(std::string a, std::string b, char c) {
	
	// these will carry the counts of chars in both strings
	int counter_a = 0;
	int counter_b = 0;
	
	// we can use the size function for strings to get the length
	int a_length = a.size();
	int b_length = b.size();
	
	// loop through both strings and count the occurrences of the character(s) c
	for (int i = 0; i < a_length; i++) {
		// we only search for 1 single character so we have to take the first entry of c
		if (a[i] == c)
			counter_a++;
	}
	
	for (int i = 0; i < b_length; i++) {
		if (b[i] == c)
			counter_b++;
	}
	
	// return a value that represents the comparison result
	// for the insertion sort we just need to know if a < b so both other cases can be 0
	if (counter_a < counter_b) 
		return 1;
	else
		return 0;
}
