#include "selection_string.hpp"

#include <iostream>

std::vector<std::string> selection_sort (std::vector<std::string> strings, char c){
	int pos_min;
	std::string temp;
	
	for (int i=0; i < strings.size()-1; i++) {
	    pos_min = i;//set pos_min to the current index of array
		
		for (int j=i+1; j < strings.size(); j++) {

			if (compare_by_frequency(strings[j], strings[pos_min], c))
				pos_min = j;
		}
		if (pos_min != i) {
			temp = strings[i];
			strings[i] = strings[pos_min];
			strings[pos_min] = temp;
		}
	}
	
	return strings;
}

int compare_to(std::string a, std::string b, char c) {
	
	// these will carry the counts of chars in both strings
	int counter_a = 0;
	int counter_b = 0;
	
	// we can use the size function for strings to get the length
	int a_length = a.size();
	int b_length = b.size();
	
	// loop through both strings and count the occurrences of the character(s) c
	for (int i = 0; i < a_length; i++) {
		// we only search for 1 single character so we have to take the first entry of c
		if (a[i] == c)
			counter_a++;
	}
	
	for (int i = 0; i < b_length; i++) {
		if (b[i] == c)
			counter_b++;
	}
	
	// return a value that represents the comparison result
	// for the insertion sort we just need to know if a < b so both other cases can be 0
	if (counter_a < counter_b) 
		return 1;
	else
		return 0;
}
