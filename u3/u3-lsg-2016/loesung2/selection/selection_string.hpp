#ifndef __SELECTION_STRING_HPP__
#define __SELECTION_STRING_HPP__

#include <vector>
#include <string>

std::vector<std::string> selection_sort(std::vector<std::string> strings, std::string c);
int compare_to(std::string a, std::string b, std::string c);

#endif
