#include "buffy_vampire_slayer.hpp"
#include <stdio.h>

// take all powers and count those of the zombies and vampires
// we return a struct that carries the counter of both creature types
creature_counts count_creatures(int N, int creature_powers[]) {
	
	// this will be our return element
	creature_counts counts;
	
	// these will carry the overall powers of both creature types
	int sum_zombie_powers = 0;
	int sum_vampire_powers = 0;
	
	int count_zombies = 0;
	int count_vampires = 0;
	
	// iterate through all creature powers and count the number of
	// even and odd entries
	for(int i = 0; i < N; i++)	{
		
		// test if an entry is even or odd, it's a little tricky
		// power % 2 == 0 -> even power but we have an if statement here
		// the if statement will go to the else part for a 0, so it goes to else for even powers
		// zombies have even powers, so we must inc the zombie counter in the else
		if(creature_powers[i] % 2) {
			count_vampires++;
			sum_vampire_powers += creature_powers[i];
		}
		else {
			count_zombies++;
			sum_zombie_powers += creature_powers[i];
		}
	}
	
	// use the counts and sums of our loop to initialize the values in our return struct
	counts.zombie_count = count_zombies;
	counts.vampire_count = count_vampires;
	
	counts.zombie_sum = sum_zombie_powers;
	counts.vampire_sum = sum_vampire_powers;
	
	return counts;
}

void swap(int *a,int *b) {
	int temp = *a;
	*a = *b;
	*b = temp;
}

// take all monsters and sort them
int *sort(int *elements, int length) {
	for(int i = 1; i < length; i++)	{
		int smallestIndex = i - 1;
		int startIndex = i;
		
		// odd
		if(!(elements[i] % 2)) {
			while((smallestIndex >= 0) && ((elements[smallestIndex] > elements[startIndex]) || (elements[smallestIndex] % 2))) {
				swap(&elements[startIndex], &elements[smallestIndex]);
				startIndex = smallestIndex;
				smallestIndex--;
			}
		}
		// even
		else {
			while((smallestIndex >= 0) && (elements[smallestIndex] > elements[startIndex]) && ((elements[smallestIndex] % 2))) {
				swap(&elements[startIndex], &elements[smallestIndex]);
				startIndex = smallestIndex;
				smallestIndex--;
			}
		}
	}
	
	return elements;
}
	
int *create_attack_plan(int N, int *elements, creature_counts counts) {
	int *attack_plan = new int[N+2];
	
	int creature_index = 0;
	for (int i = 0; i <= N; i++) {
		if (creature_index == counts.zombie_count) {
			attack_plan[creature_index] = counts.zombie_sum;
			creature_index++;
		}
		attack_plan[creature_index] = elements[i];
		creature_index++;
	}
	attack_plan[N+1] = counts.vampire_sum;
	return attack_plan;
}
