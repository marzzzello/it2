#include "buffy_vampire_slayer.hpp"
#include <iostream>

// take all powers and count those of the zombies and vampires
// we return a struct that carries the counter of both creature types
creature_counts count_creatures(int N, int creature_powers[])
{
	// this will be our return element
	creature_counts counts;

	// these will carry the overall powers of both creature types
	int sum_zombie_powers = 0;
	int sum_vampire_powers = 0;

	int count_zombies = 0; //gerade
	int count_vampires = 0;//ungerade

	//Zähle Vampire und Zombies:
	for (int i=0; i<N; i++){
		if (creature_powers[i] & 1==1){ //Maskierung mit 000...0001 letztes Bit gibt an ob gerade oder ungerade Zahl
			//ungerade
			count_vampires++;
			sum_vampire_powers+=creature_powers[i];
		}
		else{
			//gerade
			count_zombies++;
			sum_zombie_powers += creature_powers[i];
		}
	}


	counts.zombie_count = count_zombies;
	counts.vampire_count = count_vampires;
	
	counts.zombie_sum = sum_zombie_powers;
	counts.vampire_sum = sum_vampire_powers;
	
	return counts;
}

void swap(int *a,int *b)
{
	//Tausche a und b
	int tmp = *b;
	*b = *a;
	*a = tmp;
}

int *sort(int *elements, int length) {
	//Special Selection Sort:
	for (int startIndex = 0; startIndex < length - 1; ++startIndex) {
		int smallestIndex = startIndex;
		// Suche nach kleinerem Element im restlichen Array
		for (int currentIndex = startIndex + 1; currentIndex < length; ++currentIndex) {

			//Wenn beide Elemente gerade oder beide Elemente ungerade sind...
			if ((elements[currentIndex]&1 && elements[smallestIndex]&1) ||  ( !(elements[currentIndex]&1) && !(elements[smallestIndex]&1) )) {
				//...dann mache einen normalen Vergleich
				if (elements[currentIndex] < elements[smallestIndex]) 
				smallestIndex = currentIndex;
			}

			//Gerade Elemente sind immer kleiner als ungerade:
			else if(!(elements[currentIndex]&1)){
				smallestIndex=currentIndex;
			}
		}
		swap(&elements[startIndex], &elements[smallestIndex]); // Tausche Elemente
	}
	return elements;
}


int *create_attack_plan(int N, int *elements, creature_counts counts)
{

	int *attackplan = new int[N+2];
	int offset=0;
	
	for (int i=0; i<N; i++){
		if(counts.zombie_count==i){
			attackplan[i]=counts.zombie_sum;
			offset=1;	//Damit der Index um eins verschoben wird
		}
		attackplan[i+offset]=elements[i]; //Sobald der Offest 1 ist werden alle Elemente von elements[] eine Position später in attackplan[] geschrieben
	}
attackplan[N+1]=counts.vampire_sum; //Die Vampir-Summe kommt immer an letzter Stelle
	return attackplan;
}
