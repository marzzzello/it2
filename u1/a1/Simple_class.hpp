#ifndef __SIMPLE_CLASS_HPP__
#define __SIMPLE_CLASS_HPP__

class Simple_class
 {
 private:
  int attribute;	//Integer das gespeichert wird

 public:	//Methodendefinitionen:
  Simple_class(int attribute);	//Konstruktor
  void set(int attribute);		
  int get();
 };

#endif
