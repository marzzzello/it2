#include "Simple_class.hpp"

Simple_class::Simple_class(int attribute)
 {
  set(attribute);	//Übergebenes Attribut wird in Klasse gespeichert durch set() Methode
 } 


int Simple_class::get()
 {
  return attribute;		//Gebe das Attrubut zurück
 }

void Simple_class::set(int attribute)
 {
  this->attribute = attribute;	//Speichert Parameter in dieser Klasse
 }
