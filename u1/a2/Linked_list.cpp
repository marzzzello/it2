#include "Linked_list.hpp"

Linked_list::Linked_list(std::string attribut){
	this->attribut=attribut;
	this->next_ptr=nullptr;
	//setze attribut und nextpointer als Ende der Liste
}

void Linked_list::set(std::string attribut){
	this->attribut=attribut;
}

std::string Linked_list::get(){
	return this->attribut;
}

Linked_list*  Linked_list::next(){
	return this->next_ptr;
}

Linked_list *Linked_list::insert_at_index(unsigned int index, Linked_list *insertKnoten){
	
	if (index==0){
		insertKnoten->next_ptr = this; //einzufügender Knoten kommt an den Start und zeigt auf das 2. Element

		return insertKnoten; //Start der Liste wird zurückgegeben
	}

	Linked_list *searchIndex = this; //Pointer zeigt auf Element eins vor dem gesuchten Index
	for (unsigned int i=1; i<index; i++)	{
			//Schleife geht solange bis zum nächsten Listenelement bis Index erreicht wird
			searchIndex=searchIndex->next_ptr;

			//falls Index nicht mehr innerhalb der Liste gebe den nullpointer zurück
			if (searchIndex==nullptr)
				return nullptr;
	}				
	
	//Einzufügender Knoten zeigt auf nächstes Element
	insertKnoten->next_ptr = searchIndex->next_ptr;

	//Vorheriger Knoten zeigt auf einzufügenden Knoten
	searchIndex->next_ptr = insertKnoten;

	return this; //Start der Liste bleibt unverändert

}

Linked_list *Linked_list::remove_at_index(unsigned int index){
	if (index==0){
		Linked_list *ret = this->next_ptr;
		delete this;
		return ret;
	}

	Linked_list *searchIndex = this; //Pointer zeigt auf Element eins vor dem gesuchten Index
	for (unsigned int i=1; i<index; i++)	{
			//Schleife geht solange bis zum nächsten Listenelement bis Index erreicht wird
			searchIndex=searchIndex->next_ptr;

			//falls Index nicht mehr innerhalb der Liste gebe den nullpointer zurück
			if (searchIndex==nullptr)
				return nullptr;
	}				

	

	Linked_list *toDelete =searchIndex->next_ptr;
	
	//Falls zu löschendes Element nicht mehr in Liste gebe nullpointer zurück
	if (toDelete==nullptr)
		return nullptr;

	//vorheriger Knoten zeigt auf übernächstes Element
	searchIndex->next_ptr = toDelete->next_ptr;

	//Lösche Listenelement
	delete toDelete;


	return this; //Start der Liste bleibt unverändert

}
