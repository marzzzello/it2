#ifndef __LINKED_LIST_HPP__
#define __LINKED_LIST_HPP__

#include <string>

class Linked_list
{
private:
	std::string attribut;
	Linked_list *next_ptr; //Zeiger auf nächstes Objekt der Liste


public:
	Linked_list(std::string attribut);	//Konstrukor mit String als Parameter
	void set(std::string attribut);	//Setze attribut des aktuellen Listenelements
	std::string get ();	//Gebe attribut des aktuellen Listenelemts zurück
	Linked_list*  next(); //Gibt Zeiger auf nächstes Element der Liste zurück
	Linked_list *insert_at_index(unsigned int index, Linked_list *insertKnoten); //Gibt Start der Liste zurück
	Linked_list *remove_at_index(unsigned int index); //Gibt Start der Liste zurück
};

#endif
