#include "Queue.hpp"

Queue::Queue(){
	this->list=nullptr;
}

void Queue::push(std::string attribut){
	//Falls noch keine Liste erstellt, erstelle eine
	if(this->list == nullptr){
		this->list = new Linked_list (attribut);
	
	}


	else {
		//Suche das Ende der Liste
		Linked_list *searchEnd;
		while (searchEnd->next() != nullptr){
			searchEnd = searchEnd->next();
		}
		//Am Ende einfügen:
		searchEnd->insert_at_index(1, new Linked_list(attribut));

	}
}


std::string Queue::pop(){
	std::string ret = list->get();
	list = list->remove_at_index(0); //Löscht Knoten und gibt neuen Start zurück
	return ret;
}

bool Queue::has_next(){
	if (list != nullptr)
		return true;

	return false;
}