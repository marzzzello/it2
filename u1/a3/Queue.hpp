#ifndef __QUEUE_HPP__
#define __QUEUE_HPP__

#include "../a2/Linked_list.hpp"

class Queue
 {
  Linked_list *list;

 public:
  Queue();
  void push(std::string attribute);
  std::string pop();
  bool has_next();
  };

#endif

